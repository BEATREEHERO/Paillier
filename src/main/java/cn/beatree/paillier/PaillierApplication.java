package cn.beatree.paillier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient     // Eureka client
public class PaillierApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaillierApplication.class, args);
    }

}
